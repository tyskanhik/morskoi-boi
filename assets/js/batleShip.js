'use strict'

let view = {
    displayMessege: function(msg) {                                     //Вывод сообщения о статусе попадания
        let messegeArea = document.getElementById("messegeArea");
        messegeArea.innerHTML = msg;
    },

    displayHit: function(location) {                                    // Реализация присвоения классов ("hit" и "miss")
        let cell = document.getElementById(location);
        cell.setAttribute("class", "hit");
    },

    displayMiss: function(location) {
        let cell = document.getElementById(location);
        cell.setAttribute("class", "miss");
    },

}

let model = {
    boardSize: 7,   //размер поля
    numShips: 3,    //колличество кораблей
    shipLength: 3,  //длинна корабля
    shipsSunk: 0,   //текущее колличество потопленных кораблей

    ships: [{ locations: [0, 0, 0], hits: ["", "", ""] },        
            { locations: [0, 0, 0], hits: ["", "", ""] },
            { locations: [0, 0, 0], hits: ["", "", ""] }],

    fire: function(shot) {
        for (let i = 0; i < this.numShips; i++) {
            let ship = this.ships[i];
            let index = ship.locations.indexOf(shot);

            if (index >= 0) {
                ship.hits[index] = "hit";
                view.displayHit(shot);
                view.displayMessege(`Попадание`);

                if (this.isSunk(ship)) {
                    view.displayMessege(`Корабль потоплен!`);
                    this.shipsSunk++;
                }
                return true;
            }
        }
        view.displayMiss(shot);
        view.displayMessege(`Промах`);
        return false;
    },

    isSunk: function(ship) {                                //Метод проверки потоплен ли корабль
        for (let i = 0; i < this.shipLength; i++) {
            if (ship.hits[i] !== "hit") {
                return false
            }
        }
        return true;
    },

    generateShipLocations: function() {
        let locations;
        for (let i = 0; i < this.numShips; i++) {
            do {
                locations = this.generateShip();    // Генерируем новый набор позиций
            } while (this.collision(locations));    // проверка на перекрытие
            this.ships[i].locations = locations;
        }
        console.log("Ships array: ");  // Проверка массива координат кораблей
		console.log(this.ships);
    },

    generateShip: function() {
        let direction = Math.floor(Math.random() * 2);
        let row, col;

        if (direction === 1) {  // Начальная позиция для горизонтального
            row = Math.floor(Math.random() * this.boardSize);
            col = Math.floor(Math.random() * (this.boardSize - this.shipLength));
        } else {        // Начальная позиция для вертикального
            row = Math.floor(Math.random() * (this.boardSize - this.shipLength));
            col = Math.floor(Math.random() * this.boardSize);
        }

        let newShipLocations = [];
        for (let i = 0; i < this.shipLength; i++) {
            if (direction === 1) {      // Добавить в массив для горизонтального
                newShipLocations.push(row + "" + (col + i));
            } else {        // Для вертикального
                newShipLocations.push((row + i) + "" + col);
            }
        }
        return newShipLocations;
    },

    collision: function(locations) {
        for (let i = 0; i < this.numShips; i++) {
            let ship = this.ships[i];
            for ( let j = 0; j < locations.length; j++) {
                if (ship.locations.indexOf(locations[j]) >= 0) {
                    return true;
                }
            }
        }
        return false;
    },

}

let controller = {
    quanity: 0, //колличство выстрелов

    processСoordinates: function(shot) {       //Метод преобразования координатов формата "A0"
        let location = parseСoordinates(shot);
        if (location) {
            this.quanity++;
            let hit = model.fire(location);
            if (hit && model.shipsSunk === model.numShips) {
                view.displayMessege(`Победа!`);
                
                let newGame = confirm(`Победа! \n Колличество выстрелов: ${this.quanity} \n точность стрельбы ${3 / this.quanity} \n 
                    Желаете начать новую игру?`);
                    if (!!newGame){
                    document.location.href = "index.html";
                    } else {
                        document.location.href = "game.html";
                    }
            }
        }

    },
}

function parseСoordinates(shot) {
    let alphabet = ["A", "B", "C", "D", "E", "F", "G"];

    if (shot === null || shot.length !==2) {
        alert(`Введите корректные координаты`);
    } else {
        let firstChar = shot.charAt(0);
        let row = alphabet.indexOf(firstChar);
        let column = shot.charAt(1);

        if (isNaN(row) || isNaN(column)) {
            console.log(`второе значение некорректно`);
            
        } else if (row < 0 || row >= model.boardSize ||
                    column < 0 || column >= model.boardSize) {
                        console.log(`первое значение некорректно`);
        } else {
            return row + column;
        }
    }
    return null;
}

function handleFireButton() {
    let shotInput = document.getElementById("shotInput");
    let shot = shotInput.value;

    controller.processСoordinates(shot);
    shotInput.value = "";
}

function handleKeyPress (e) {
    let fireButton = document.getElementById("fireButton");
    if (e.keyCode === 13) {
        fireButton.click();
        return false;
    }
}

window.onload = init;

function init() {
    let fireButton = document.getElementById("fireButton");
    fireButton.onclick = handleFireButton;
    let shotInput = document.getElementById("shotInput");
    shotInput.onkeypress = handleKeyPress;

    model.generateShipLocations(); // Вызов метода генерирующего позиции кораблей
}

